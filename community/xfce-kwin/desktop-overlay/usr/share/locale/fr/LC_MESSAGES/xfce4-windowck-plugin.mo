��    '      T  5   �      `     a     s  
   �     �     �     �  	   �     �  *   �     %     E     J     S  ^   q  .   �  ,   �     ,     I     b     ~     �     �  &   �     �     �     �  $         %     ,     D     Z     b     i     o     v          �     �  $  �     �     �     �     �  !     %   $     J  !   V  ;   x  0   �     �     �  &   �  �   	  D   �	  A   �	  !   A
  (   c
  !   �
      �
     �
     �
  9        @     T     Z  &   l  	   �     �     �     �     �     �     �  	         
                            $                              !                	   '   %                            #                             
             &            "                         <b>Appearance</b> <b>Behaviour</b> Alignment: Button layout: Control active windows. Control maximized windows. Directory Display the title in two lines Get in sync with the window manager theme. Logout on close button desktop. Menu Padding: Place the menu icon on right. Put the buttons id in the desired order.
Example: [HMC]
H=Hide, M=Maximize/unMaximize, C=Close Put the maximized window buttons on the panel. Put the maximized window title on the panel. Show the buttons on desktop. Show the full title name Show the plugin on desktop. Show the window Icon. Show the window menu. Subtitle font: Sync the font with the window manager. Themes usable Title Title font: Unable to open the following url: %s Width: Window Header - Buttons Window Header - Title at most center chars expand fixed to left pixels right Project-Id-Version: 0.1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-03-22 18:39+0100
Last-Translator: Cédric Leporcq <cedl38@gmail.com>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
 <b>Apparence</b> <b>Comportement</b> Alignement : Disposition des boutons : Contrôler les fenêtres actives. Contrôler les fenêtres maximisées. Répertoire Afficher le titre sur deux lignes Se synchroniser avec le theme du gestionnaire de fenêtres. Se déconnecter avec le bouton fermer du bureau. Menu Espacement : Placer l'icône du menu sur la droite. Placer l'identifiant des boutons dans l'ordre choisi.
Exemple : [HMC]
H=Hide (minimiser), M=Maximize/unMaximize (maximiser/démaximiser), C=Close (fermer). Placer les boutons des fenêtres maximisées sur le tableau de bord. Placer le titre des fenêtres maximisées sur le tableau de bord. Afficher le plugin sur le bureau. Afficher le titre complet des fenêtres. Afficher le plugin sur le bureau. Afficher l'icône des fenêtres. Afficher le menu des fenêtres. Police du sous-titre : Synchroniser la police avec le gestionnaire de fenêtres. Thèmes utilisables Titre Police du titre : Impossible d'ouvrir l'url suivant : %s Largeur : Entête des Fenêtres - Boutons Entête des Fenêtres - Titre au plus centré caractères étendre fixée à gauche pixels droite 