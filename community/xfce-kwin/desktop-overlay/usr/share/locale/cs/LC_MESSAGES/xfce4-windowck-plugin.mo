��    '      T  5   �      `     a     s  
   �     �     �     �  	   �     �  *   �     %     E     J     S  ^   q  .   �  ,   �     ,     I     b     ~     �     �  &   �     �     �     �  $         %     ,     D     Z     b     i     o     v          �     �  '  �     �     �     �     �     �          4  #   <  '   `  <   �     �  
   �     �  u   �  2   o	  1   �	     �	     �	     
     #
     8
     P
  (   c
     �
     �
     �
  &   �
  	   �
     �
            
   #     .     5     A     J     P     X                  $                              !                	   '   %                            #                             
             &            "                         <b>Appearance</b> <b>Behaviour</b> Alignment: Button layout: Control active windows. Control maximized windows. Directory Display the title in two lines Get in sync with the window manager theme. Logout on close button desktop. Menu Padding: Place the menu icon on right. Put the buttons id in the desired order.
Example: [HMC]
H=Hide, M=Maximize/unMaximize, C=Close Put the maximized window buttons on the panel. Put the maximized window title on the panel. Show the buttons on desktop. Show the full title name Show the plugin on desktop. Show the window Icon. Show the window menu. Subtitle font: Sync the font with the window manager. Themes usable Title Title font: Unable to open the following url: %s Width: Window Header - Buttons Window Header - Title at most center chars expand fixed to left pixels right Project-Id-Version: 0.1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-05-11 10:51+0100
Last-Translator: Pavel Zlámal <zlamal.pavel@gmail.com>
Language-Team: Czech
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
 <b>Vzhled</b> <b>Chování</b> Zarovnání: Rozložení tlačítek: Ovládat aktivní okna. Ovládat maximalizovaná okna. Složka Zobrazit titulek ve dvou řádcích Synchronizovat se stylem Správce oken. Odhlásit se pomocí tlačítka Zavřít bez okna na ploše. Nabídka Odsazení: Umístit ikonu nabídky vpravo. Vložte id tlačítek v očekávaném pořadí.
Příklad: [HMC]
H=Minimalizovat, M=Maximalizovat/Obnovit, C=Zavřít Vloží tlačítka pro ovládání okna do panelu. Vloží titulek maximalizovaných oken do panelu. Zobrazit tlačítka na ploše. Zobrazit plný text titulku Zobrazit na ploše. Zobrazit ikonu okna. Zobrazit nabídku okna. Písmo podtitulku: Synchronizovat písmo se Správcem oken. Dostupné styly Titulek Písmo titulku: Nelze otevřít následující url: %s Šířka: Hlavička okna - Tlačítka Hlavička okna - Titulek nejvíce uprostřed znaků roztáhnout přesně vlevo pixelů vpravo 