��    '      T  5   �      `     a     s  
   �     �     �     �  	   �     �  *   �     %     E     J     S  ^   q  .   �  ,   �     ,     I     b     ~     �     �  &   �     �     �     �  $         %     ,     D     Z     b     i     o     v          �     �  �  �          5     O  &   i  4   �  :   �      	  <   	  F   R	  O   �	     �	     �	  6    
  �   7
  T   �
  Z   7  =   �  4   �  =     '   C  #   k     �  7   �     �            2   4     g  "   u  (   �     �     �     �     �       
   -     8     I                  $                              !                	   '   %                            #                             
             &            "                         <b>Appearance</b> <b>Behaviour</b> Alignment: Button layout: Control active windows. Control maximized windows. Directory Display the title in two lines Get in sync with the window manager theme. Logout on close button desktop. Menu Padding: Place the menu icon on right. Put the buttons id in the desired order.
Example: [HMC]
H=Hide, M=Maximize/unMaximize, C=Close Put the maximized window buttons on the panel. Put the maximized window title on the panel. Show the buttons on desktop. Show the full title name Show the plugin on desktop. Show the window Icon. Show the window menu. Subtitle font: Sync the font with the window manager. Themes usable Title Title font: Unable to open the following url: %s Width: Window Header - Buttons Window Header - Title at most center chars expand fixed to left pixels right Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-09-30 21:10+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Last-Translator: https://github.com/zluca
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
Language: ru_RU
 <b>Внешний вид</b> <b>Поведение</b> Выравнивание: Расположение кнопок: Работать с активными окнами. Работать с развёрнутыми окнами. Директория Показывать заголовок в две линии Синхронизироваться с системной темой. Выйти при закрытии кнопок на рабочем столе. Меню Отступ: Поместить иконку окна справа. Разместите id кнопок в желаемом порядке.
Например: [HMC]
H=Скрыть, M=Свернуть/Развернуть, C=закрыть Поместить кнопки развёрнутого окна на панель. Поместить заголовок развёрнутого окна на панель. Показать кнопки на рабочем столе. Показывать полный заголовок Показать плагин на рабочем столе. Показать иконку окна. Показать меню окна. Шрифт субтитров: Использовать системный шрифт. Доступные темы Заголовок Шрифт заголовка: Невозможно открыть адрес: %s Ширина: Шапка окна - кнопки Шапка окна - Заголовок не более чем по центру символов расширить до фиксировать к слева пикселов справа 