��          �      <      �     �     �  
   �     �     �  ,        ?     [  $   q     �     �     �     �     �     �     �     �  f  �     4     C     R     `     z  :   �     �     �       
   .  
   9     D  	   K     U     ]     e     n               
                                         	                                             <b>Appearance</b> <b>Behaviour</b> Alignment: Control active windows. Control maximized windows. Put the maximized window title on the panel. Show the plugin on desktop. Show the window Icon. Unable to open the following url: %s Width: at most center chars expand fixed to left right Project-Id-Version: xfce4-windowck-plugin 0.1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-04-02 12:54+0100
Last-Translator: Cédric Leporcq <cedl38@gmail.com>
Language-Team: Italian
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.4
 <b>Aspetto</b> <b>Aspetto</b> Allineamento: Gestisci finestre attive. Gestisci finestre massimizate. Mostra il titolo della finestra massimizzata sul pannello. Mostra il plugin sul desktop. Mostra l'icona della finestra. Impossibile aprire l'url: %s Larghezza: al massimo centro caratteri espandi espandi sinistra destra 