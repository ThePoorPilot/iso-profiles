��    '      T  5   �      `     a     s  
   �     �     �     �  	   �     �  *   �     %     E     J     S  ^   q  .   �  ,   �     ,     I     b     ~     �     �  &   �     �     �     �  $         %     ,     D     Z     b     i     o     v          �     �  $  �     �     �     �     �     �                      6     U     t     {     �  M   �  /   �  )   	     G	     c	     v	     �	     �	     �	     �	     �	     �	     �	     
     
     &
     =
     T
     [
     b
     i
  	   p
     z
     �
     �
                  $                              !                	   '   %                            #                             
             &            "                         <b>Appearance</b> <b>Behaviour</b> Alignment: Button layout: Control active windows. Control maximized windows. Directory Display the title in two lines Get in sync with the window manager theme. Logout on close button desktop. Menu Padding: Place the menu icon on right. Put the buttons id in the desired order.
Example: [HMC]
H=Hide, M=Maximize/unMaximize, C=Close Put the maximized window buttons on the panel. Put the maximized window title on the panel. Show the buttons on desktop. Show the full title name Show the plugin on desktop. Show the window Icon. Show the window menu. Subtitle font: Sync the font with the window manager. Themes usable Title Title font: Unable to open the following url: %s Width: Window Header - Buttons Window Header - Title at most center chars expand fixed to left pixels right Project-Id-Version: 0.1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-06-02 16:08+0800
Last-Translator: Dylan Chu<chdy.uuid@gmail.com>
Language-Team: Chinese
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
 <b>外观</b> <b>行为</b> 对齐: 按钮顺序(HMC): 控制活动窗口 控制最大化的窗口 目录 用两行显示标题 和窗口管理器主题同步 在关闭桌面按钮时注销 菜单 边距: 把菜单图标放在右侧 自定义按钮顺序
例如: [HMC]
H=最小化, M=最大化/恢复, C=关闭 把最大化窗口的控制按钮放在panel上 把最大化窗口的标题放在panel上 在桌面时也显示按钮 显示整个标题 在桌面时也显示 显示窗口图标 显示窗口菜单 子标题字体: 和窗口管理器同步字体 可用主题 标题 标题字体: 不能打开这个url: %s 宽度: Window Header - 按钮 Window Header - 标题 最大 中间 字符 扩展 固定为 左侧 像素 右侧 