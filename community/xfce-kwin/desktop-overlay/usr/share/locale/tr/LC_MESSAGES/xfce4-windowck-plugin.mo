��    '      T  5   �      `     a     s  
   �     �     �     �  	   �     �  *   �     %     E     J     S  ^   q  .   �  ,   �     ,     I     b     ~     �     �  &   �     �     �     �  $         %     ,     D     Z     b     i     o     v          �     �  %  �     �     �  	   �     �     �  '        <  '   B  -   j  /   �     �     �  '   �  e   �  0   c	  2   �	  "   �	     �	      
     )
     F
     c
  3   x
     �
     �
     �
  "   �
  
              1     P     Y     `  	   i     s     y     }     �                  $                              !                	   '   %                            #                             
             &            "                         <b>Appearance</b> <b>Behaviour</b> Alignment: Button layout: Control active windows. Control maximized windows. Directory Display the title in two lines Get in sync with the window manager theme. Logout on close button desktop. Menu Padding: Place the menu icon on right. Put the buttons id in the desired order.
Example: [HMC]
H=Hide, M=Maximize/unMaximize, C=Close Put the maximized window buttons on the panel. Put the maximized window title on the panel. Show the buttons on desktop. Show the full title name Show the plugin on desktop. Show the window Icon. Show the window menu. Subtitle font: Sync the font with the window manager. Themes usable Title Title font: Unable to open the following url: %s Width: Window Header - Buttons Window Header - Title at most center chars expand fixed to left pixels right Project-Id-Version: 0.1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-11 21:18+0100
Last-Translator: Yaşar Çiv <yasarciv67@gmail.com>
Language-Team: Turkish
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
 <b>Görünüm</b> <b>Davranış</b> Hizalama: Düğme düzeni: Etkin pencereleri kontrol et. Büyütülmüş pencereleri kontrol et. Dizin Başlıkları iki satırda görüntüle Pencere yöneticisi temasıyla senkronize ol. Masaüstünde kapat düğmesinden çıkış yap Menü Dolgu: Menü simgesini sağ tarafa yerleştir. Düğmeleri istenen sıraya koyun.
Örnek: [HMC]
H=Küçült, M=Büyüt/Büyütmeyi geri al, C=Kapat  Büyütülmüş pencere düğmelerini panele koy Büyütülmüş pencere başlığını panele koy. Düğmeleri masaüstünde göster. Tam başlık adını göster. Eklentiyi masaüstünde göster. Pencere simgelerini göster. Pencere menüsünü göster. Altyazı yazı tipi: Yazı tipini pencere yöneticisi ile senkronize et. Kullanılabilir temalar Başlık Başlık yazı tipi: Aşağıdaki URL açılamıyor: %s Genişlik: Pencere Başlığı - Düğmeler Pencere Başlığı - Başlık en fazla merkez karakter genişlet sabit sol piksel sağ 